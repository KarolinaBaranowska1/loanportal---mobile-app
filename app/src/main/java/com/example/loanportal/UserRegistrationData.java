package com.example.loanportal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * This is a class that represents registration dsta.
 * In different words: Data Transfer Object for my User.
 *
 * @Data - does getters and setters for me
 * @Bulder - it s a design template, the annotation
 * "invokes" it and make it possible to quickly creates
 * object so that I dont have to make a constructor for my class.
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserRegistrationData
{
    private String login;
    private String name;
    private String surname="ggggggggg";
    private String password;
    private String role="hhhhhhhh";
    private String email;
    private String yearOfBirth;

}
