package com.example.loanportal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginData
{
    String acces_token;
    String token_type;
    String refresh_token;
    String expires_in;
    String scope;
    String login;
    String jti;
}
