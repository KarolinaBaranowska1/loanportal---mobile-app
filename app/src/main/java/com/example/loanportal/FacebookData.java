package com.example.loanportal;

import com.facebook.AccessToken;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FacebookData
{
        private String firstName;
        private String lastName;
        private String middleName;
        private String fbUserId;
        private String accessToken;
}
