package com.example.loanportal;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class SocialMediaService
{
    private static final String JSON_BASE_URL = "https://jwtservice.herokuapp.com";
    private static final String JSON_BASE_URL2 = "localhost:8080"; //https://riskloanapp.herokuapp.com/

    public static SocialMediaService instance;
    private UserServiceInterface service;
    private FacebookServiceInterface serviceFb;

    private SocialMediaService()
    {
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();
        final Retrofit retrofit = new Retrofit.Builder().baseUrl(JSON_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())// adapter
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        final Retrofit retrofit2 = new Retrofit.Builder().baseUrl(JSON_BASE_URL2)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())// adapter
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        service = retrofit.create(UserServiceInterface.class);
        serviceFb = retrofit2.create(FacebookServiceInterface.class);
    }

    public static SocialMediaService getInstance()
    {
        if (instance == null)
        {
            instance = new SocialMediaService();
        }
        return instance;
    }

    public Observable<Void> sendFacebookDataTrial(FacebookData facebokData)
    {
        return service.sendFacebookDataTrial(facebokData);
    }

    public Observable<Void> sendFacebookData(FacebookData facebokData)
    {
        return serviceFb.sendFacebookData(facebokData);
    }

}
