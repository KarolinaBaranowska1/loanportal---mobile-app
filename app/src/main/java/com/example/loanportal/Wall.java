package com.example.loanportal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.facebook.AccessToken;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Wall extends Activity
{
    private Button profileButton;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.wall);
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        profileButton = findViewById(R.id.profile);

        Toast.makeText(this,"Zalogowany jako -> "+MainActivity.loginData.login, Toast.LENGTH_LONG).show();

        profileButton.setOnClickListener(v ->
        {
            this.startActivity(new Intent(this,Profile.class));
            com.facebook.Profile profile = com.facebook.Profile.getCurrentProfile();
            try
            {
                SocialMediaService.getInstance().sendFacebookData(FacebookData.builder()
                        .firstName(profile.getFirstName())
                        .lastName(profile.getLastName())
                        .middleName(profile.getMiddleName())
                        .fbUserId(profile.getId())
                        .accessToken(accessToken.getToken())
                        .build())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(w -> System.out.println("HSHSHSHSHSHSHHSHSHHS")
                        );
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }




        });


     }


    private Context getActivity()
    {
        return this;
    }

}
