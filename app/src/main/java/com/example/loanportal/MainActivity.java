package com.example.loanportal;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.appevents.AppEventsLogger;

import java.util.HashMap;
import java.util.Map;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    public static LoginData loginData;
    private Button registerButton;
    private Button logInButton;
    private EditText email;
    private EditText password;
    private Map<String, String> params = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        registerButton = findViewById(R.id.registerButton);
        logInButton = findViewById(R.id.logInButton);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Register.class);
                MainActivity.this.startActivity(intent);
            }
        });

        logInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Wall.class);
                Intent intent2 = new Intent(MainActivity.this, MainActivity.class);
                //MainActivity.this.startActivity(intent);
                RegistrationService.getInstance().sendLoginData("password", password.getText().toString(), email.getText().toString())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(w -> {
                                    loginData = w;
                                    MainActivity.this.startActivity(intent);
                                }, //w to co zwraca mikorserwis do logowanie jeśli onSucces
                                y -> {
                                    Toast.makeText(MainActivity.this,"Zly login", Toast.LENGTH_LONG).show();
                                } //onError, onFinally puste ofc
                        );
            }
        });


    }
}
