package com.example.loanportal;

import retrofit2.http.Body;
import retrofit2.http.POST;
import rx.Observable;

public interface FacebookServiceInterface
{
    @POST("/fbdata")
    Observable<Void> sendFacebookData(@Body FacebookData facebookData);
}
