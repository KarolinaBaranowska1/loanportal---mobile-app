package com.example.loanportal;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class RegistrationService
{
    private static final String JSON_BASE_URL = "https://jwtservice.herokuapp.com";

    public static RegistrationService instance;
    private UserServiceInterface service;

    private RegistrationService()
    {
        final Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY).create();

        final Retrofit retrofit = new Retrofit.Builder().baseUrl(JSON_BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())// adapter
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        service = retrofit.create(UserServiceInterface.class);
    }

    public static RegistrationService getInstance()
    {
        if (instance == null)
        {
            instance = new RegistrationService();
        }
        return instance;
    }

    public Observable<Void> sendData(UserRegistrationData userRegistrationData)
    {
        return service.sendData(userRegistrationData);
    }

    public Observable<LoginData> sendLoginData(String grant_type,String password,String username)
    {
        return service.sendLoginData(grant_type,password,username);
    }


}
