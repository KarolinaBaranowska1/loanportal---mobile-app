package com.example.loanportal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class Register extends Activity
    {

    private Button registerButton;
    private EditText nameInput;
    private EditText surnameInput;
    private EditText emailInput;
    private EditText dateOfBirth;
    private EditText passwdInput;
    private EditText repeatedPasswd;

    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration);
        registerButton = findViewById(R.id.registerButton);
        nameInput = findViewById(R.id.nameInput);
        surnameInput = findViewById(R.id.surnameInput);
        emailInput = findViewById(R.id.emailInput);
        dateOfBirth = findViewById(R.id.dateOfBirth);
        passwdInput = findViewById(R.id.passwdInput);
        repeatedPasswd = findViewById(R.id.repeatedPasswd);


        Intent intent = new Intent(Register.this, Wall.class);
        Intent intent2 = new Intent(Register.this, MainActivity.class);
        //Register.this.startActivity(intent);
        registerButton.setOnClickListener(v ->
        {
        try
            {
            RegistrationService.getInstance().sendData(UserRegistrationData.builder()
                    .email(emailInput.getText().toString())
                    .name(nameInput.getText().toString())
                    .login(nameInput.getText().toString())
                    .role("asdasdasda")
                    .surname("Kowalski")
                    .password(passwdInput.getText().toString())
                    .yearOfBirth(dateOfBirth.getText().toString())
                    .build())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(w ->
                            {
                            Toast.makeText(this,"Zarejestrowano!!",Toast.LENGTH_LONG).show();
                            Register.this.startActivity(intent2);
                            }, //w to co zwraca
                            // mikorserwis do logowanie jeśli onSucces
                            y -> Toast.makeText(this,"Nie zarejestrowano",Toast.LENGTH_LONG).show() //onError, onFinally puste ofc
                    );
            }
        catch (Exception e)
            {
            e.printStackTrace();
            }
        });
        }

    private Context getActivity()
        {
        return this;
        }

    }
