package com.example.loanportal;

import java.util.Map;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

public interface UserServiceInterface
{
    @POST("/register")
        //bo bede wysylac do mikroserwisu
    Observable<Void> sendData(@Body UserRegistrationData userRegistrationData);

    @POST("/fbdataTrial")
    Observable<Void> sendFacebookDataTrial(@Body FacebookData facebookData);

    //@Body, ponieważ chce cały obiekt, a nie pojedyncze query
    @POST("/oauth/token")
    @Headers("Authorization: Basic YXJhczphcmFz")
    @FormUrlEncoded
    Observable<LoginData> sendLoginData(@Field("grant_type") String grant_type,@Field("password") String password,@Field("username") String username);
}

